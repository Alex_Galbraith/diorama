﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ThreeGridEditorWindow : EditorWindow {
    private ThreeGrid instance;

    private enum EditMode {
        PLACE,
        DESTROY,
        REPLACE,
        FILL_HOR,
        BRUSH
    }

    public int ToPlace = 0;
    public int Radius = 3;

    private bool Active;

    private EditMode Mode = EditMode.REPLACE;

    [MenuItem("Window/ThreeGrid")]
    static void Init() {
        ThreeGridEditorWindow window = (ThreeGridEditorWindow)EditorWindow.GetWindow(typeof(ThreeGridEditorWindow));
        window.Show();
    }

    // Window has been selected
    void OnBecameVisible() {
        // Remove delegate listener if it has previously
        // been assigned.
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
        // Add (or re-add) the delegate.
        SceneView.onSceneGUIDelegate += this.OnSceneGUI;
        Active = true;
    }

    private void OnBecameInvisible() {
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
        Active = false;
    }

    void OnDestroy() {
        // When the window is destroyed, remove the delegate
        // so that it will no longer do any drawing.
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
    }
    private void Update() {
        
    }

    private Vector3 lastSelectionPos;
    private bool MouseOneDown;
    private double LastAction = 0;

    private void OnSceneGUI(SceneView sceneView) {
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        bool selectionActive = false;
        if (instance == null)
            return;
        Vector2 guiPosition = Event.current.mousePosition;
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        Vector3 mouseWorld = Event.current.mousePosition;
        mouseWorld.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mouseWorld.y;
        mouseWorld = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(mouseWorld);


        //Handles.BeginGUI();
        //Handles.DrawLine(ray.origin, ray.origin + ray.direction * 100f);
        MeshCollider mc = instance.GetComponent<MeshCollider>();
        if (mc != null) {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100)) {
                Vector3 hPos = hit.point;
                if (Mode == EditMode.PLACE || Mode == EditMode.FILL_HOR || Mode == EditMode.BRUSH)
                    hPos += hit.normal * 0.2f;
                else
                    hPos -= hit.normal * 0.2f;
                Vector3 pos = instance.SnapPoint(hPos);
                lastSelectionPos = pos;
                selectionActive = true;
                Handles.DrawWireCube(pos, instance.transform.lossyScale * 1.1f);
            }
        }
        //Handles.EndGUI();
        HandleUtility.Repaint();

        Event mEvent = Event.current;

        if (mEvent != null && mEvent.isMouse) {
            //Get Mouse Down
            if (mEvent.type == EventType.MouseDown && mEvent.button == 0 && selectionActive) {
                MouseOneDown = true;
                LastAction = EditorApplication.timeSinceStartup;
                switch (Mode) {
                    case EditMode.PLACE:
                    case EditMode.REPLACE:
                        instance.PlaceBlock(lastSelectionPos, ToPlace);
                        break;
                    case EditMode.DESTROY:
                        instance.DestroyBlock(lastSelectionPos);
                        break;
                    case EditMode.FILL_HOR:
                        instance.FillHor(lastSelectionPos, ToPlace);
                        break;
                    case EditMode.BRUSH:
                        instance.PlaceSphere(lastSelectionPos, ToPlace, Radius);
                        break;
                }
                
            }

            //Get Mouse Up
            if (mEvent.type == EventType.MouseUp && mEvent.button == 0) {
                MouseOneDown = false;
            }
            //Get Mouse Move
            if (mEvent.type == EventType.MouseDrag && mEvent.button == 0 && selectionActive) {
                if (EditorApplication.timeSinceStartup - LastAction > 0.1f) {
                    LastAction = EditorApplication.timeSinceStartup;
                    switch (Mode) {
                        case EditMode.PLACE:
                            instance.PlaceBlock(lastSelectionPos, ToPlace);
                            break;
                        case EditMode.DESTROY:
                            instance.DestroyBlock(lastSelectionPos);
                            break;
                    }
                }
            }
        }
    }

    private void OnGUI() {
        ThreeGrid[] grids = Selection.GetFiltered<ThreeGrid>(SelectionMode.TopLevel);
        if (grids.Length > 0)
            instance = grids[0];
        Mode = (EditMode)GUILayout.SelectionGrid((int)Mode, new string[] { "Place", "Destroy", "Replace", "Fill Hor", "Brush" }, 3);
        //ToPlace = EditorGUILayout.IntField(ToPlace);
        if (Mode == EditMode.BRUSH) {
            GUIContent radLabel = new GUIContent("Radius");
            Radius = (int)EditorGUILayout.Slider(radLabel, Radius, 1, 20);
        }
        GUILayout.BeginHorizontal();
        float width = EditorGUIUtility.currentViewWidth;
        int perRow = (int)(width / 110);
        int r = 0;
        foreach (var b in instance.blocks) {
            
            if (r >= perRow) {
                r = 0;
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
            }
            var o = GUILayout.Width(100);
            var w = GUILayout.Height(100);
            GUIContent BlockLabel;
            if (b.SideTextures[0] != null)
                BlockLabel = new GUIContent(b.SideTextures[0]);
            else
                BlockLabel = new GUIContent(b.name);
            if (GUILayout.Button(BlockLabel, o, w))
                ToPlace = b.ID;
            r++;
        }
        GUILayout.EndHorizontal();

    }


    void OnInspectorUpdate() {
        Repaint();
    }
}
