#ifdef DISTANCE_FUNC
#define DISTANCE_FUNC


bool sphereHit (float3 p, float3 _Center, float _Radius)
{
    return distance(p,_Centre) < _Radius;
}


float sphereDistance(float3 p)
{
	return distance(p, _Centre) - _Radius;
}



#endif