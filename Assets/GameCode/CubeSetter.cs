﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class CubeSetter : MonoBehaviour {
    public string ParameterName;
    public ReflectionProbe probe;
    public Material material;
	// Use this for initialization
	void Update () {
        material.SetTexture(name, probe.texture);
    }
	
}
