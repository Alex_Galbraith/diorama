﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Light))]
public class FogLight : MonoBehaviour
{
    public Vector2Int shadowMapSize = new Vector2Int(256,256);
    private new Light light;
    public RenderTexture shadowMapRenderTexture;
    private CommandBuffer _copyShadowmapCommandBuffer;
    // Start is called before the first frame update
    void Awake()
    {
        light = GetComponent<Light>();
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Init() {
        shadowMapRenderTexture = new RenderTexture(shadowMapSize.x, shadowMapSize.y, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
        shadowMapRenderTexture.Create();
        RenderTargetIdentifier shadowMapRenderTextureIdentifier = BuiltinRenderTextureType.CurrentActive;
        _copyShadowmapCommandBuffer = new CommandBuffer {
            name = "Aura : Copy light's shadowmap"
        };
        _copyShadowmapCommandBuffer.SetShadowSamplingMode(shadowMapRenderTextureIdentifier, ShadowSamplingMode.RawDepth);
        _copyShadowmapCommandBuffer.Blit(shadowMapRenderTextureIdentifier, new RenderTargetIdentifier(shadowMapRenderTexture));
        light.AddCommandBuffer(LightEvent.AfterShadowMap, _copyShadowmapCommandBuffer);
    }
}
