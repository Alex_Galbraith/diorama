﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UI{
    public class SetMaterialFloat : MonoBehaviour
    {
        public Material material;
        
        public string propertyName;
        [Header("Optional Value Inject")]
        public Slider slider; 
        public InputField input; 

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            if (slider!=null)
                slider.value = material.GetFloat(propertyName);
            if (input!=null)
                input.text = ""+material.GetFloat(propertyName);
        }

        public void SetValue(float f){
            material.SetFloat(propertyName, f);
        }
    }
}
