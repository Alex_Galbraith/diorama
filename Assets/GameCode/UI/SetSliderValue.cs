﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[RequireComponent(typeof(Slider))]
/// <summary>
/// Sets the slider value to that of the specified variable
/// </summary>
public class SetSliderValue : MonoBehaviour
{
    public string VarName;
    public MonoBehaviour Object;
    // Start is called before the first frame update
    void Start()
    {
        //Hideous reflection to get variable
        GetComponent<Slider>().value = (float)Object.GetType().GetField(VarName).GetValue(Object);
    }
}
