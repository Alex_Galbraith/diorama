﻿Shader "Fog/FogBlit"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _FogTex ("Fog Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

			#define SmoothRad 1
//			#define SMOOTH

            sampler2D _MainTex, _FogTex;
			fixed2 _UV_PPX;

            fixed4 frag (v2f inp) : SV_Target
            {
                half4 d = 0;
#ifdef SMOOTH
				for (int i = -SmoothRad; i <= SmoothRad; i++) {
					for (int j = -SmoothRad; j <= SmoothRad; j++) {
						d += tex2D(_FogTex, inp.uv + fixed2(i*_UV_PPX.x, j*_UV_PPX.y));
					}
				}
				d /= (SmoothRad * 2 + 1)* (SmoothRad * 2 + 1);
#else
				d = tex2D(_FogTex, inp.uv);
#endif


				return float4(d.rgb + tex2D(_MainTex, inp.uv).rgb*(d.a),1);
            }
            ENDCG
        }
    }
}
