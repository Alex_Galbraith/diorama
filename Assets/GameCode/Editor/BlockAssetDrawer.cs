﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(BlockAsset))]
public class BlockAssetDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.PropertyField(position, property, label, true);
        EditorGUI.BeginProperty(position, label, property);
        SerializedObject SO = new SerializedObject(property.objectReferenceValue);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        EditorGUI.indentLevel = 0;
        var idRect = new Rect(position.x, position.y + position.height/2, 30, position.height/2);
        var nameRect = new Rect(position.x + 30, position.y + position.height/2, 60, position.height/2);
        EditorGUI.PropertyField(idRect, SO.FindProperty("ID"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, SO.FindProperty("BlockName"), GUIContent.none);
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return base.GetPropertyHeight(property, label) * 2;
    }
}
