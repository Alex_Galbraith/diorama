﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[RequireComponent(typeof(ColorPicker))]
/// <summary>
/// Sets the slider value to that of a light
/// </summary>
public class SetColorPickerFromLight : MonoBehaviour
{
    public Light Light;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ColorPicker>().CurrentColor = Light.color;
    }
}
