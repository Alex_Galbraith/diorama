﻿
Shader "Unlit/VolumeFirst"
{
	Properties
	{
		_Radius("Radius", Float) = 1
		_Opacity("Opacity", Float) = 0.1
		_Center("Position", Vector) = (0,0,0,0)
		_Noise("Noise", 2D) = "white"{}
		_AmbientColor("Ambient", Color) = (0,0,0,0)

	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Cull Off ZWrite Off ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			float _Radius, _Opacity;
			fixed4 _Noise_ST, _AmbientColor;
			float3 _Center;

			sampler2D _CameraDepthTexture, _Noise;

			#define STEPS 250
			#define MIN_DISTANCE 0.1


			float getNoise(float3 p) {
				float2 uv = float2(p.x , p.y ) + _Time * fixed2(0.5, -1.2);
				uv = TRANSFORM_TEX(uv, _Noise);
				float tot = (tex2Dlod(_Noise, float4(uv, 0, 0)).r * 2 - 0.5) / 6;
				uv = float2(p.x, p.z) + _Time * fixed2(0.5, -1.2);
				uv = TRANSFORM_TEX(uv, _Noise);
				tot+= (tex2Dlod(_Noise, float4(uv, 0, 0)).r * 2 - 0.5) / 6;
				uv = float2(p.y, p.z) + _Time * fixed2(0.5, -1.2);
				uv = TRANSFORM_TEX(uv, _Noise);
				tot += (tex2Dlod(_Noise, float4(uv, 0, 0)).r * 2 - 0.5) / 6;

				uv = float2(p.y, p.x) + _Time * fixed2(0.5, -1.2);
				uv = TRANSFORM_TEX(uv, _Noise);
				tot += (tex2Dlod(_Noise, float4(uv, 0, 0)).r * 2 - 0.5) / 6;
				uv = float2(p.z, p.x) + _Time * fixed2(0.5, 1.2);
				uv = TRANSFORM_TEX(uv, _Noise);
				tot += (tex2Dlod(_Noise, float4(uv, 0, 0)).r * 2 - 0.5) / 6;
				uv = float2(p.z, p.y) + _Time * fixed2(0.5, -1.2);
				uv = TRANSFORM_TEX(uv, _Noise);
				tot += (tex2Dlod(_Noise, float4(uv, 0, 0)).r * 2 - 0.5)/6;

				return tot;

			}

			
			float sphereHit(float3 p, float3 _Center, float _Radius)
			{
				float d = distance(p, _Center);
				float edge = min((1 -  d / _Radius) * 2, 1);
				edge = edge * edge * edge;
				float e = edge * max(getNoise(p)*2-1,0);
				return e * e;
				//return edge;
			}
			

			float sphereDistance(float3 p, float3 _Center, float _Radius)
			{
				return distance(p, _Center) - _Radius;
			}



			fixed4 raymarch(float3 position, float3 direction, float maxDepth)
			{
				float dist = 0;
				float4 accum = _AmbientColor;
				float4 lastSample = 0;
				float energy = 1;
				for (int i = 0; i < STEPS; i++)
				{
					float distance = sphereDistance(position, _Center, _Radius);
					
					if (distance < MIN_DISTANCE) {
						distance = MIN_DISTANCE;
						float hit = sphereHit(position, _Center, _Radius) *_Opacity;
						fixed4 samp = fixed4(1* hit * energy, 1* hit * energy, 1* hit * energy, hit);
						float shadow = 1;
						float3 oldPos = position;
						float oldEnergy = energy;
						for (int i = 0; i < STEPS/10 * step(0.1, hit); i++)
						{
							float distance2 = sphereDistance(position, _Center, _Radius);
							if (distance2 > 0 || shadow < 0)
								break;
							float mhit = sphereHit(position, _Center, _Radius);
							shadow *= (1-mhit);
							position += _WorldSpaceLightPos0 * MIN_DISTANCE * 10;
						}
						energy = oldEnergy;
						position = oldPos;

						shadow = max(shadow, 0);
						energy *= (1-hit);
						accum +=  fixed4(((lastSample + samp) / 2).rgb * shadow, ((lastSample + samp) / 2).a * distance);
						//accum +=  fixed4(((lastSample + samp) / 2).rgb, ((lastSample + samp) / 2).a * distance);
						lastSample = samp;
						
					}
					else {
						lastSample = 0;
					}

					dist += distance;
					if (dist > maxDepth)
						return accum;
					if (accum.a > 1 ) {
						return accum;
					}
					
					position += distance * direction;
				}
				//accum.a=step(0.01, accum.a);
				return accum;
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float4 pos : SV_POSITION;
				float4 screenPos: TEXCOORD2;
				float eyeZ : TEXCOORD3;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.wpos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.screenPos = ComputeScreenPos(o.pos);
				COMPUTE_EYEDEPTH(o.eyeZ);
				return o;
			}
			

			fixed4 frag(v2f i) : SV_Target
			{
				float3 worldPosition = i.wpos;
				float3 viewDirection = normalize(i.wpos - _WorldSpaceCameraPos);
				
				float screenZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)));
				fixed4 d = (raymarch(worldPosition, viewDirection, screenZ - i.eyeZ));
				//d = 0;
				d= clamp(d, 0, 1);
				return d;
			}


			



			ENDCG
		}
	}
}
