﻿using System.Collections.Generic;
using UnityEngine;
using System;


public class ThreeGrid : MonoBehaviour {
    public BlockAsset[] blocks;

    private void Start() {
        Rebuild();
    }

    public Vector3Int Dimensions;
    [SerializeField]
    public int[][][] BlockIDs;
    [SerializeField]
    [HideInInspector]
    public int[] BlockShadows;
    [SerializeField]
    [HideInInspector]
    public int[] BlockIDsFlat;
    private Vector3 BlockSize = new Vector3(1, 1, 1);

    private BlockMaterial[] GroupedMaterials;

    private BlockRenderData[] BlockRenderDatas;


    private static Vector3Int[] directions = { Vector3Int .right, Vector3Int.up, new Vector3Int(0,0,1), Vector3Int.left, Vector3Int.down, new Vector3Int(0, 0, -1) };
    private static Vector3Int[] vertDirections = { Vector3Int .right, new Vector3Int(0,0,1), Vector3Int.left, new Vector3Int(0, 0, -1) };


    private struct BlockRenderData{
        public bool SimpleRender;
        public int[] uvIDs;
        public int materialGroup;
    }

    Dictionary<int, BlockMaterial> BlockToBlockMaterial;
    Dictionary<Material, BlockMaterial> materialToBlockMaterial;
    private class BlockMaterial {
        public Texture2D Atlas;
        public Material Material;
        public Rect[] UVs;
    }

    public Vector3Int WorldPosToGrid(Vector3 wpos) {
        wpos -= transform.position;
        BlockSize = transform.lossyScale;
        return new Vector3Int((int)Mathf.Floor(wpos.x / BlockSize.x), (int)Mathf.Floor(wpos.y / BlockSize.y), (int)Mathf.Floor(wpos.z / BlockSize.z));
    }


    #region Editing

    public bool CheckBounds(Vector3Int pos) {
        int x = pos.x;
        int y = pos.y;
        int z = pos.z;

        if (x < 0) {
            return false;
        }
        if (y < 0) {
            return false;
        }
        if (z < 0) {
            return false;
        }

        if (x >= Dimensions.x) {
            return false;
        }
        if (y >= Dimensions.y) {
            return false;
        }
        if (z >= Dimensions.z) {
            return false;
        }

        return true;
    }

    public void PlaceBlock(Vector3 pos, int block) {
        if (block > blocks.Length - 1){
            Debug.LogError("INVALID BLOCK: " + block); return;}
        Vector3Int p = WorldPosToGrid(pos);
        if (!CheckBounds(p))
            return;
        DestroyBlockInternal(p);
        BlockIDs[p.x][p.y][p.z] = block;
        BuildMesh();
        flatten();
    }

    private void DestroyBlockInternal(Vector3Int p) {
        BlockIDs[p.x][p.y][p.z] = 0;
    }

    public void DestroyBlock(Vector3 pos) {
        Vector3Int p = WorldPosToGrid(pos);
        if (!CheckBounds(p))
            return;
        DestroyBlockInternal(p);
        BuildMesh();
        flatten();
    }

    public void PlaceSphere(Vector3 pos, int block, int radius) {
        if (block > blocks.Length - 1){
            Debug.LogError("INVALID BLOCK: " + block); return;}
        Vector3Int p = WorldPosToGrid(pos);
        for (int x = 0; x<radius*2; x++) {
            for (int y = 0; y < radius * 2; y++) {
                for (int z = 0; z < radius * 2; z++) {
                    Vector3Int n = p + new Vector3Int(x-radius, y - radius, z - radius);

                    if (CheckBounds(n)) {
                        if (Vector3.Distance(p, n) < radius) {
                            DestroyBlockInternal(n);
                            BlockIDs[n.x][n.y][n.z] = block;
                        }
                    }
                }
            }
        }
        BuildMesh();
        flatten();
    }

    public void FillHor(Vector3 pos, int block) {
        if (block > blocks.Length - 1){
            Debug.LogError("INVALID BLOCK: " + block); return;}
        if (block == 0)
            return;
        Vector3Int p = WorldPosToGrid(pos);
        Queue<Vector3Int> q = new Queue<Vector3Int>();
        q.Enqueue(p);
        while (q.Count > 0) {
            p = q.Dequeue();
            DestroyBlockInternal(p);
            BlockIDs[p.x][p.y][p.z] = block;
            foreach (Vector3Int d in vertDirections) {
                Vector3Int n = p + d;
                if (CheckBounds(n)) { 
                    if (BlockIDs[n.x][n.y][n.z] == 0) {
                        q.Enqueue(n);
                        BlockIDs[n.x][n.y][n.z] = -1;
                    }
                }
            }
        }
        BuildMesh();
        flatten();
    }
    #endregion


    #region Editor

#if UNITY_EDITOR
    [SerializeField]
    private Vector3Int OldDimensions;
    

    private void OnValidate() {
        Array.Sort(blocks, CompareBlocks);
    }

    public static int CompareBlocks(BlockAsset a, BlockAsset b) {
        return a.ID - b.ID;
    }


#endif

    public void Rebuild() {
    #if UNITY_EDITOR
        if (OldDimensions != Dimensions) {
            OldDimensions = Dimensions;
            BlockIDs = new int[Dimensions.x][][];
            for (int x = 0; x < Dimensions.x; x++) {
                BlockIDs[x] = new int[Dimensions.y][];
                for (int y = 0; y < Dimensions.y; y++) {
                    BlockIDs[x][y] = new int[Dimensions.z];
                }
            }

            //Build bottom layer
            for (int x = 0; x < Dimensions.x; x++) {
                for (int z = 0; z < Dimensions.z; z++) {
                    BlockIDs[x][0][z] = 1;
                }
            }
        }
    #endif
        if (BlockIDs == null) {
            unFlatten();
        }
        flatten();

        //Build texture mapping
        int blockID = 0;
        int matCounter = 0;
        List<List<Texture2D>> texSets = new List<List<Texture2D>>();
        List<BlockMaterial> mats = new List<BlockMaterial>();
        materialToBlockMaterial = new Dictionary<Material, BlockMaterial>();
        BlockRenderDatas = new BlockRenderData[blocks.Length];

        int[] sideIDs = new int[blocks.Length * 6];
        foreach (var b in blocks) {
            if (b.SimpleRender) {
                Material m = b.Materials[0];
                if (m == null) {
                    blockID++;
                    continue;
                }
                if (!materialToBlockMaterial.ContainsKey(m)) {
                    //TODO: Optimise
                    materialToBlockMaterial[m] = new BlockMaterial {
                        Atlas = new Texture2D(4096, 4096, TextureFormat.ARGB32, false),
                        Material = m
                    };
                    mats.Add(materialToBlockMaterial[m]);
                    matCounter++;
                    texSets.Add(new List<Texture2D>());
                }
                
                BlockMaterial bm = materialToBlockMaterial[m];
                BlockRenderDatas[blockID] = new BlockRenderData {
                    materialGroup = matCounter - 1,
                    SimpleRender = true,
                    uvIDs = new int[6]
                };

                int side = 0;
                foreach (var i in b.SideTextures) {
                    texSets[matCounter - 1].Add(i);
                    BlockRenderDatas[blockID].uvIDs[side] = texSets[matCounter - 1].Count - 1;
                    side++;
                }
            }
            blockID++;
        }

        GroupedMaterials = mats.ToArray();
        Rect[] UVs = null;
        Debug.Log("Packing Textures");
        for (int i = 0; i<GroupedMaterials.Length; i++) {
            var mat = GroupedMaterials[i];
            Debug.Log("Packing texture "+i);
            UVs = mat.Atlas.PackTextures(texSets[i].ToArray(), 20, 4096, false);
            mat.Atlas.wrapMode = TextureWrapMode.Clamp;
            uPaddingBleed.BleedEdges(mat.Atlas, 20, UVs, false);
            Vector2 dims = new Vector2(mat.Atlas.width,mat.Atlas.height);
            for (int j = 0; j<UVs.Length; j++) {
                Rect r = UVs[j];
                r.width -= 8/dims.x;
                r.height -= 8 / dims.y;
                r.position += new Vector2(4 / dims.x, 4 / dims.y);
                UVs[j] = r;
            }
            
            mat.UVs = UVs;
        }
        Debug.Log("Finished Packing Textures");
        BuildMesh();
    }


    #endregion

    public Vector3 SnapPoint(Vector3 pos, bool center = true) {
        Vector3 ip = WorldPosToGrid(pos);
        ip.x *= transform.lossyScale.x;
        ip.y *= transform.lossyScale.y;
        ip.z *= transform.lossyScale.z;

        ip += transform.position;
        if (center)
            ip += transform.lossyScale * 0.5f;
        return ip;
    }
    
    private void Update() {

    }



    #region MeshTechnical
    private Vector3[] GenerateCubeVerts(Vector3 pos, bool[] render, int count) {
        /*Vector3[] o = new Vector3[8];

        o[0] = pos;
        o[1] = pos + new Vector3(1, 0, 0);
        o[2] = pos + new Vector3(0, 1, 0);
        o[3] = pos + new Vector3(1, 1, 0);

        o[4] = pos + new Vector3(0, 0, 1);
        o[5] = pos + new Vector3(1, 0, 1);
        o[6] = pos + new Vector3(0, 1, 1);
        o[7] = pos + new Vector3(1, 1, 1);*/
        int i = 0;
        

        Vector3[] o = new Vector3[count * 4];
        //FRONT
        if (render[0]) {
            o[0] = pos;
            o[1] = pos + new Vector3(1, 0, 0);
            o[2] = pos + new Vector3(0, 1, 0);
            o[3] = pos + new Vector3(1, 1, 0);
            i++;
        }
        //BACK
        if (render[1]) {
            o[4 * i]     = pos + new Vector3(1, 0, 1);
            o[4 * i + 1] = pos + new Vector3(0, 0, 1);
            o[4 * i + 2] = pos + new Vector3(1, 1, 1);
            o[4 * i + 3] = pos + new Vector3(0, 1, 1);
            i++;
        }
        //Top
        if (render[2]) {
            o[4 * i] = pos + new Vector3(0, 1, 0);
            o[4 * i + 1] = pos + new Vector3(1, 1, 0);
            o[4 * i + 2] = pos + new Vector3(0, 1, 1);
            o[4 * i + 3] = pos + new Vector3(1, 1, 1);
            i++;
        }
        //bot
        if (render[3]) {
            o[4 * i] = pos + new Vector3(1, 0, 0);
            o[4 * i + 1] = pos + new Vector3(0, 0, 0);
            o[4 * i + 2] = pos + new Vector3(1, 0, 1);
            o[4 * i + 3] = pos + new Vector3(0, 0, 1);
            i++;
        }
        //left
        if (render[4]) {
            o[4 * i] = pos + new Vector3(0, 0, 1);
            o[4 * i + 1] = pos + new Vector3(0, 0, 0);
            o[4 * i + 2] = pos + new Vector3(0, 1, 1);
            o[4 * i + 3] = pos + new Vector3(0, 1, 0);
            i++;
        }
        //right
        if (render[5]) {
            o[4 * i] = pos + new Vector3(1, 0, 0);
            o[4 * i + 1] = pos + new Vector3(1, 0, 1);
            o[4 * i + 2] = pos + new Vector3(1, 1, 0);
            o[4 * i + 3] = pos + new Vector3(1, 1, 1);
        }


        return o;
    }

    private Vector3[] GenerateCubeNorms(Vector3 pos, bool[] render, int count) {
        /*Vector3[] o = new Vector3[8];

        o[0] = pos;
        o[1] = pos + new Vector3(1, 0, 0);
        o[2] = pos + new Vector3(0, 1, 0);
        o[3] = pos + new Vector3(1, 1, 0);

        o[4] = pos + new Vector3(0, 0, 1);
        o[5] = pos + new Vector3(1, 0, 1);
        o[6] = pos + new Vector3(0, 1, 1);
        o[7] = pos + new Vector3(1, 1, 1);*/
        int i = 0;


        Vector3[] o = new Vector3[count * 4];
        //FRONT
        if (render[0]) {
            o[0] = Vector3.back;
            o[1] = Vector3.back;
            o[2] = Vector3.back;
            o[3] = Vector3.back;
            i++;
        }
        //BACK
        if (render[1]) {
            o[4 * i] = Vector3.forward;
            o[4 * i + 1] = Vector3.forward;
            o[4 * i + 2] = Vector3.forward;
            o[4 * i + 3] = Vector3.forward;
            i++;
        }
        //Top
        if (render[2]) {
            o[4 * i] = Vector3.up;
            o[4 * i + 1] = Vector3.up;
            o[4 * i + 2] = Vector3.up;
            o[4 * i + 3] = Vector3.up;
            i++;
        }
        //bot
        if (render[3]) {
            o[4 * i] = Vector3.down;
            o[4 * i + 1] = Vector3.down;
            o[4 * i + 2] = Vector3.down;
            o[4 * i + 3] = Vector3.down;
            i++;
        }
        //left
        if (render[4]) {
            o[4 * i] = Vector3.left;
            o[4 * i + 1] = Vector3.left;
            o[4 * i + 2] = Vector3.left;
            o[4 * i + 3] = Vector3.left;
            i++;
        }
        //right
        if (render[5]) {
            o[4 * i] = Vector3.right;
            o[4 * i + 1] = Vector3.right;
            o[4 * i + 2] = Vector3.right;
            o[4 * i + 3] = Vector3.right;
        }


        return o;
    }

    private int[] GenerateCubeTris(int startAt, bool[] render, int count) {

        int[] o = new int[count * 6];
        /*
        //FRONT
        o[0] = startAt;
        o[1] = startAt + 2;
        o[2] = startAt + 1;
        //Tri 2
        o[3] = startAt + 2;
        o[4] = startAt + 3;
        o[5] = startAt + 1;

        //BACK
        o[6] = startAt + 4;
        o[7] = startAt + 6;
        o[8] = startAt + 5;
        //Tri 4
        o[9] = startAt + 6;
        o[10] = startAt + 7;
        o[11] = startAt + 5;

        //LEFT
        o[12] = startAt + 4;
        o[13] = startAt + 6;
        o[14] = startAt + 0;
        //Tri 6
        o[15] = startAt + 6;
        o[16] = startAt + 2;
        o[17] = startAt + 0;

        //RIGHT
        o[18] = startAt + 1;
        o[19] = startAt + 3;
        o[20] = startAt + 5;
        //Tri 8
        o[21] = startAt + 3;
        o[22] = startAt + 7;
        o[23] = startAt + 5;

        //TOP
        o[24] = startAt + 2;
        o[25] = startAt + 6;
        o[26] = startAt + 3;
        //Tri 10
        o[27] = startAt + 6;
        o[28] = startAt + 7;
        o[29] = startAt + 3;

        //BOT
        o[30] = startAt + 4;
        o[31] = startAt + 0;
        o[32] = startAt + 5;
        //Tri 12
        o[33] = startAt + 0;
        o[34] = startAt + 1;
        o[35] = startAt + 5;*/
        int i = 0;
        for (int j = 0; j<6; j++) {
            if (!render[j])
                continue;
            o[i * 6] = startAt + i * 4;
            o[i * 6 + 1] = startAt + i * 4 + 2;
            o[i * 6 + 2] = startAt + i * 4 + 1;
            o[i * 6 + 3] = startAt + i * 4 + 2;
            o[i * 6 + 4] = startAt + i * 4 + 3;
            o[i * 6 + 5] = startAt + i * 4 + 1;
            i++;
        }

        return o;
    }

    private Vector2[] GenerateUVs(BlockRenderData block, bool[] render, int count) {
        Vector2[] uvs = new Vector2[count * 4];
        int i = 0;
        Rect[] rects = GroupedMaterials[block.materialGroup].UVs;
        for (int j = 0; j < 6; j++) {
            if (!render[j])
                continue;
            Rect r = rects[block.uvIDs[j]];//BlockUVs[SideIds[BlockID * 6 + j]];

            uvs[i*4] = r.min;
            uvs[i * 4 + 1] = new Vector2(r.xMax, r.yMin);
            uvs[i * 4 + 2] = new Vector2(r.xMin, r.yMax);
            uvs[i * 4 + 3] = new Vector2(r.xMax, r.yMax);
            i++;
        }
        return uvs;
    }
    private bool[] DrawSides(int x, int y, int z, out int count) {
        bool[] o = { false, false, false, false, false, false };
        int mb = BlockIDs[x][y][z];
        BlockAsset block = blocks[mb];
        count = 0;
        if (x - 1 < 0) {
            o[4] = true;
        } 
        else {
            int b = BlockIDs[x - 1][ y][ z];
            bool op = blocks[b].OpaqueFaces[5];
            o[4] = !op;
        }

        if (y - 1 < 0) { 
            o[3] = true;
        } 
        else {
            int b = BlockIDs[x ][ y - 1][ z];
            bool op = blocks[b].OpaqueFaces[2];
            o[3] = !op;
        }

        if (z - 1 < 0) {
            o[0] = true;
        }
        else {
            int b = BlockIDs[x][ y][ z - 1];
            bool op = blocks[b].OpaqueFaces[1];
            o[0] = !op;
        }

        if (x + 1 >= Dimensions.x) {
            o[5] = true;
        }
        else {
            int b = BlockIDs[x + 1][ y][ z];
            bool op = blocks[b].OpaqueFaces[4];
            o[5] = !op;
        }
        if (y + 1 >= Dimensions.y) {
            o[2] = true;
        }
        else {
            int b = BlockIDs[x][ y + 1][ z];
            bool op = blocks[b].OpaqueFaces[3];
            o[2] = !op;
        }
        if (z + 1 >= Dimensions.z) { 
            o[1] = true;
        }
        else {
            int b = BlockIDs[x][ y ][ z + 1];
            bool op = blocks[b].OpaqueFaces[0];
            o[1] = !op;
        }
        foreach (bool b in o)
            if (b)
                count++;
        return o;
    }
    #endregion

    public void BuildMesh() {
        Mesh m = new Mesh();
        m.subMeshCount = GroupedMaterials.Length;




        MeshFilter mf = GetComponent<MeshFilter>();

        if (Application.isEditor) {
            if (mf.sharedMesh != null)
                DestroyImmediate(mf.sharedMesh);
        }
        else
            Destroy(mf.sharedMesh);

        List<Vector3> vertList = new List<Vector3>();
        
        List<int>[] triList = new List<int>[GroupedMaterials.Length];
        for(int i = 0; i < triList.Length; i++) {
            triList[i] = new List<int>();
        }

        List<Vector2> uvList = new List<Vector2>();
        List<Vector3> normList = new List<Vector3>();
        int verts = 0;

        for (int x = 0; x < Dimensions.x; x++) {
            for (int y = 0; y < Dimensions.y; y++) {
                for (int z = 0; z < Dimensions.z; z++) {
                    var bs = BlockRenderDatas[BlockIDs[x][y][z]];
                    if (BlockIDs[x][y][z] == 0)
                        continue;
                    if (bs.SimpleRender) {
                        bool[] sides = DrawSides(x, y, z, out int count);
                        vertList.AddRange(GenerateCubeVerts(new Vector3(x, y, z), sides, count));
                        triList[bs.materialGroup].AddRange(GenerateCubeTris(verts, sides, count));
                        uvList.AddRange(GenerateUVs(bs, sides, count));
                        normList.AddRange(GenerateCubeNorms(new Vector3(x, y, z), sides, count));
                        verts = vertList.Count;
                    }
                }
            }
        }
        m.SetVertices(vertList);
        MeshRenderer mr = GetComponent<MeshRenderer>();
        Material[] mToSet = new Material[GroupedMaterials.Length];
        for (int i = 0; i < GroupedMaterials.Length; i++) {
            m.SetTriangles(triList[i], i);
            mToSet[i] = GroupedMaterials[i].Material;
            mToSet[i].SetTexture("_MainTex", GroupedMaterials[i].Atlas);
        }
        mr.sharedMaterials = mToSet;

        m.SetUVs(0,uvList);
        m.SetNormals(normList);
        mf.sharedMesh = m;
        
        MeshCollider collider = GetComponent<MeshCollider>();
        if (collider != null) {
            collider.sharedMesh = m;
        }
    }

    private void unFlatten() {
        BlockIDs = new int[Dimensions.x][][];
        for (int x = 0; x < Dimensions.x; x++) {
            BlockIDs[x] = new int[Dimensions.y][];
            for (int y = 0; y < Dimensions.y; y++) {
                BlockIDs[x][y] = new int[Dimensions.z];
            }
        }
        for (uint x = 0; x < Dimensions.x; x++) {
            for (uint y = 0; y < Dimensions.y; y++) {
                for (uint z = 0; z < Dimensions.z; z++) {
                    BlockIDs[x][y][z] = BlockIDsFlat[x + Dimensions.x * y + Dimensions.x * Dimensions.y * z];
                }
            }
        }
    }

    private void flatten() {

        BlockShadows = new int[Dimensions.x * Dimensions.y * Dimensions.z];
        BlockIDsFlat = new int[Dimensions.x * Dimensions.y * Dimensions.z];
        for (uint x = 0; x < Dimensions.x; x++) {
            for (uint y = 0; y < Dimensions.y; y++) {
                for (uint z = 0; z < Dimensions.z; z++) {
                    int b = BlockIDs[x][y][z];
                    byte o = 1;
                    if (b > 0)
                        o = 0;
                    BlockShadows[x + Dimensions.x * y + Dimensions.x * Dimensions.y * z] = o;
                    BlockIDsFlat[x + Dimensions.x * y + Dimensions.x * Dimensions.y * z] = b;
                }
            }
        }

    }
    private ComputeBuffer cb;
    public ComputeBuffer BlockDataToBuffer() {
        if (cb == null)
            cb = new ComputeBuffer(Dimensions.x * Dimensions.y * Dimensions.z, sizeof(int));

        cb.SetData(BlockShadows);
        return cb;
    }

    

    

}
