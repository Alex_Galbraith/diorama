﻿Shader "Custom/HeightGradiented" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RampAddTex("Add Gradient Texture", 2D) = "black" {}
		_RampMultTex("Mult Gradient Texture", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_MaxHeight("Max Height", Float) = 50.0
		[MaterialToggle] _Caustics("Do caustics", Float) = 1
		_WaterHeight("Water height", Float) = 0
		_CausticTex("Caustic Texture", 2D) = "white"
		//_FogAcc("FogAcc", 3D) = "transparent"{}
		_Dimensions("Dimensions", Vector) = (25,50,25)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

#include "UnityCG.cginc"
#include "AutoLight.cginc"
#include "Lighting.cginc"

		sampler2D _MainTex;
		sampler2D _RampAddTex;
		sampler2D _RampMultTex, _CausticTex;

		half _Glossiness, _Caustics;
		fixed _WaterHeight;
		half _Metallic;
		half _MaxHeight;
		fixed4 _Color;

		sampler3D _FogAcc;

		float3 _Dimensions;

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : NORMAL;

		};
		struct v2f
		{
			float4 pos : SV_POSITION;
			float3 worldNormal : TEXCOORD0;
			float2 uv : TEXCOORD4;
			float4 worldPos : TEXCOORD6;
		};

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
		};



#ifndef POINT
#ifndef SPOT
#ifndef POINT_COOKIE
#ifndef DIRECTIONAL_COOKIE
		float4x4 unity_WorldToLight;
#endif
#endif
#endif
#endif

	

		void surf (Input IN, inout SurfaceOutputStandard o) {

			float2 flow = float2(-2, 4);
			float wrappedTime =  frac(_Time * 1);

			// Albedo comes from a texture tinted by color
			fixed4 c = tex2Dlod (_MainTex, float4(IN.uv_MainTex,0,0)) * _Color;
			
			float ramp = clamp(IN.worldPos.y / _MaxHeight,0,0.99);
			fixed4 mult = tex2D(_RampMultTex, float2(ramp, 0));
			//mult = mult / 10;
			fixed4 add = tex2D(_RampAddTex, float2(ramp, 0));
			c = (c + add)*mult;

			////////////////
			fixed doCaustics = step(IN.worldPos.y, _WaterHeight) * _Caustics;
			doCaustics = doCaustics * (_WaterHeight-IN.worldPos.y) / _WaterHeight;
			float4 lpos = float4(IN.worldPos.x + flow.x * wrappedTime, IN.worldPos.y, IN.worldPos.z + flow.y * wrappedTime, (1 -(IN.worldPos.y - _WaterHeight) * .05));
			float4 lcoord = mul(unity_WorldToLight, lpos);
			fixed t = abs(frac(_Time[1] * 0.5) - 0.5) * 2;
			fixed caustic =  1 - (tex2D(_CausticTex, lcoord.xy / lcoord.w + 0.5 ).r *0.5 + 1 );
			fixed caustic2 = 1 - (tex2D(_CausticTex, lcoord.xy / lcoord.w).r *0.5 + 1);
			///////////////

			float3 upos = IN.worldPos;
			float4 acc = (tex3Dlod(_FogAcc, float4(upos.x / _Dimensions.x, upos.y / _Dimensions.y, upos.z / _Dimensions.z, 100)));

			o.Albedo = c.rgb * (1 - lerp(caustic,caustic2,t) * doCaustics) * acc;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
