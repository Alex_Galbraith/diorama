﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class UIRotatable : MonoBehaviour
    {
         [System.Serializable]
        public class RotEvent : UnityEvent<Vector3> {}
        public RotEvent SendOnStart;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            Vector3 e = transform.eulerAngles;
            SendOnStart.Invoke(e);
        }
        public void SetRotation(Vector3 v){
            transform.eulerAngles = v;
        }
    }

}
