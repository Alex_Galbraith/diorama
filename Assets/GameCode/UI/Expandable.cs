﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class Expandable : MonoBehaviour
    {
        public float ContractedHeight = 100;
        public float ExpandedHeight = 100;
        public bool Expanded = false;
        private RectTransform myRect;
        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            myRect = GetComponent<RectTransform>();
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            Toggle(Expanded);
        }
        public void Toggle(){
            Toggle(!Expanded);
        }

        public void Toggle(bool t){
            Expanded = t;
            Vector2 s = myRect.sizeDelta;
            s.y = t?ExpandedHeight:ContractedHeight;
            myRect.sizeDelta = s;
        }
    }
}

