﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[RequireComponent(typeof(ColorPicker))]
/// <summary>
/// Sets the slider value to that of the specified variable
/// </summary>
public class SetColorPickerValue : MonoBehaviour
{
    public string VarName;
    public MonoBehaviour Object;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ColorPicker>().CurrentColor = (Color)Object.GetType().GetField(VarName).GetValue(Object);
    }
}
