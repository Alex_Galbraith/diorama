﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "3Grid/Block", fileName = "block.asset")]
[Serializable]
public class BlockAsset : ScriptableObject {
    public Mesh mesh;
    public Material[] Materials;
    [SerializeField]
    public string BlockName;
    [SerializeField]
    public ushort ID;
    [Tooltip("Top, Bottom, Left, Right, Front, Back")]
    public bool[] OpaqueFaces = {true, true, true, true, true, true};
    [Tooltip("Does this block require extra mesh data")]
    public bool SimpleRender = true;
    [Tooltip("Does this block require extra components")]
    public bool SimpleFunction = true;

    public Texture2D[] SideTextures = { null, null, null, null, null, null};
}
