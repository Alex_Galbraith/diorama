﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(BlockAsset))]
public class BlockAssetEditor : Editor {
    private static readonly string[] SIDES = {"FRONT","BACK","TOP","BOT","LEFT","RIGHT"};

    public override void OnInspectorGUI() {
        //base.OnInspectorGUI();
        EditorGUI.BeginChangeCheck();
        BlockAsset target = (BlockAsset)this.target;
        target.ID = (ushort)EditorGUILayout.IntField("ID", target.ID);
        target.BlockName = EditorGUILayout.TextField("Block Name", target.BlockName);
        target.SimpleFunction = EditorGUILayout.Toggle("Simple Function", target.SimpleFunction);
        target.SimpleRender = EditorGUILayout.Toggle("Simple Render", target.SimpleRender);
        if (target.SimpleRender) {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Simple Render Fields", EditorStyles.largeLabel);
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Opaque Sides", EditorStyles.boldLabel);
            EditorGUI.indentLevel = 1;
            for (int i = 0; i < 6; i++) {
                target.OpaqueFaces[i] = EditorGUILayout.Toggle(SIDES[i], target.OpaqueFaces[i]);
            }
            EditorGUI.indentLevel = 0;
            EditorGUILayout.Separator();

            if(target.Materials == null || target.Materials.Length == 0) {
                target.Materials = new Material[1];
            }

            target.Materials[0] = (Material)EditorGUILayout.ObjectField("Material", target.Materials[0], typeof(Material), false);

            EditorGUILayout.LabelField("Side Textures", EditorStyles.boldLabel);
            EditorGUI.indentLevel = 0;
            float old = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 50;
            for (int i = 0; i < 3; i++) {
                EditorGUILayout.BeginHorizontal();
                target.SideTextures[2*i] = (Texture2D)EditorGUILayout.ObjectField(SIDES[2 * i], target.SideTextures[2*i], typeof(Texture2D), false);
                target.SideTextures[2*i+1] = (Texture2D)EditorGUILayout.ObjectField(SIDES[2 * i + 1], target.SideTextures[2*i + 1], typeof(Texture2D), false);
                EditorGUILayout.EndHorizontal();
            }
            EditorGUIUtility.labelWidth = old;
            EditorGUI.indentLevel = 0;

        }
        else {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Complex Render Fields", EditorStyles.largeLabel);
            EditorGUILayout.Space();
            target.mesh = (Mesh)EditorGUILayout.ObjectField("Mesh",target.mesh, typeof(Mesh), false);
            var arr = serializedObject.FindProperty("Materials");
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(arr, true);
            if (EditorGUI.EndChangeCheck())
                serializedObject.ApplyModifiedProperties();
        }
        serializedObject.ApplyModifiedProperties();

        if (EditorGUI.EndChangeCheck()) {
            EditorUtility.SetDirty(this.target);
        }

        AssetDatabase.SaveAssets();
    }
}
