﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFogCompute : MonoBehaviour {
    public float Density = 0.2f;

    public void SetDensity(float f){
        Density = f;
    }
    public Color Absorbtion = Color.white * 0.05f;
    [HideInInspector]
    public Color Extinction = Color.black* 0.95f;
    public void SetExtinction(Color f){
        Absorbtion = Color.white - f;
        Extinction = f;
    }
    [Header("Texture settings")]
    public Vector2Int fogTextureSize = new Vector2Int(1920 / 2, 1080 / 2);
    public Vector3Int FogDataSize = new Vector3Int(25, 36, 25);
    [Header("Reference Injection")]
    public ComputeShader FogComputeShader;
    public ThreeGrid grid;
    public new Light light;
    public Transform worldTrans, cameraTransform;

    private RenderTexture FogDensity, LightAccum, FogRender;
    private RenderTexture SmoothSampler;
    [Header("Textures")]
    public Texture2D PhaseGradient;
    public Texture2D FogNoise;
    [Header("Material Injection")]
    public Material FogMaterial;
    public Material FogBlit;
    [Header("Generation")]
    public float heightDivision = 1.8f;
    public float noiseStr = 1;
    public float heightNoiseStr = 5f;
    public float depthSpread = 30f;
    public float baseFog = 0f;

    Camera cam;

    
    [Header("Lights")]
    public FogLight directional;

    public float HeightDivision { get => heightDivision; set => heightDivision = value; }
    public float NoiseStr { get => noiseStr; set => noiseStr = value; }
    public float HeightNoiseStr { get => heightNoiseStr; set => heightNoiseStr = value; }
    public float DepthSpread { get => depthSpread; set => depthSpread = value; }
    public float BaseFog { get => baseFog; set => baseFog = value; }

    // Use this for initialization
    void Start () {
        
        cam = this.GetComponent<Camera>();
        FogDensity = new RenderTexture(FogDataSize.x, FogDataSize.y, FogDataSize.z) {
            useMipMap = false,
            dimension = UnityEngine.Rendering.TextureDimension.Tex3D,
            depth = 0,
            volumeDepth = FogDataSize.z,
            enableRandomWrite = true,
            antiAliasing = 1,
            filterMode = FilterMode.Bilinear,
            format = RenderTextureFormat.ARGBHalf,
        };
        FogDensity.Create();

        LightAccum = new RenderTexture(FogDataSize.x, FogDataSize.y, FogDataSize.z) {
            useMipMap = false,
            dimension = UnityEngine.Rendering.TextureDimension.Tex3D,
            format = RenderTextureFormat.ARGBHalf,
            depth = 0,
            volumeDepth = FogDataSize.z,
            enableRandomWrite = true,
            filterMode = FilterMode.Bilinear,
            antiAliasing = 1,
        };
        LightAccum.Create();

        SmoothSampler = new RenderTexture(FogDataSize.x, FogDataSize.y, FogDataSize.z) {
            useMipMap = false,
            dimension = UnityEngine.Rendering.TextureDimension.Tex3D,
            format = RenderTextureFormat.ARGBHalf,
            depth = 0,
            volumeDepth = FogDataSize.z,
            enableRandomWrite = true,
            filterMode = FilterMode.Bilinear,
            antiAliasing = 1,
        };
        SmoothSampler.Create();

        FogRender = new RenderTexture(fogTextureSize.x, fogTextureSize.y, 0) {
            useMipMap = false,
            dimension = UnityEngine.Rendering.TextureDimension.Tex2D,
            format = RenderTextureFormat.ARGBHalf,
            filterMode = FilterMode.Bilinear,
            antiAliasing = 1,
            wrapMode = TextureWrapMode.Clamp
        };
        FogRender.Create();

    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void RunShader() {
        FogComputeShader.SetVector("texSize", new Vector4(FogDataSize.x, FogDataSize.y, FogDataSize.z, 0));
        int fogKern = FogComputeShader.FindKernel("GenFog");
        FogComputeShader.SetTexture(fogKern, "Result", FogDensity);
        FogComputeShader.SetFloat("time", Time.time);
        FogComputeShader.SetFloat("heightDivision", heightDivision);
        FogComputeShader.SetFloat("noiseStr", noiseStr);
        FogComputeShader.SetFloat("heightNoiseStr", heightNoiseStr);
        FogComputeShader.SetFloat("depthSpread", depthSpread);
        FogComputeShader.SetFloat("baseFog", baseFog);
        FogComputeShader.SetFloat("Density", Density);
        FogComputeShader.SetVector("Extinction", Absorbtion);
        FogComputeShader.Dispatch(fogKern, Mathf.CeilToInt(FogDataSize.x / 5f), Mathf.CeilToInt(FogDataSize.y / 10f), Mathf.CeilToInt(FogDataSize.z / 5f));

        int clearKern = FogComputeShader.FindKernel("ClearLight");
        FogComputeShader.SetTexture(clearKern, "CollectResult", LightAccum);
        FogComputeShader.SetTexture(clearKern, "CopyTo", SmoothSampler);
        FogComputeShader.Dispatch(clearKern, 20, 40, 20);

        int collectKern = FogComputeShader.FindKernel("CollectLightStepped");
        FogComputeShader.SetTexture(collectKern, "Result", FogDensity);
        FogComputeShader.SetTexture(collectKern, "CollectResult", LightAccum);
        FogComputeShader.SetTexture(collectKern, "AccumSampler", SmoothSampler);
        FogComputeShader.SetVector("lightDirection", (Vector4)light.transform.forward);
        FogComputeShader.SetVector("cameraDirection", (Vector4)cameraTransform.forward);
        FogComputeShader.SetVector("lightColor", Vector4.one);
        FogComputeShader.SetFloat("STEP_SIZE", 1);
        FogComputeShader.SetFloat("BLOCK_SIZE", grid.Dimensions.x / (float)FogDataSize.x);
        FogComputeShader.SetInt("STEPS", 100);
        FogComputeShader.SetVector("position", (Vector4)worldTrans.position);
        FogComputeShader.SetTexture(collectKern, "PhaseTex", PhaseGradient);
        Vector3 dims = new Vector3(LightAccum.width, LightAccum.height, LightAccum.volumeDepth);
        FogComputeShader.SetVector("dims", dims);
       
        FogComputeShader.SetVector("blockDims", new Vector3(grid.Dimensions.x, grid.Dimensions.y, grid.Dimensions.z));
        FogComputeShader.Dispatch(collectKern, Mathf.CeilToInt(FogDataSize.x / 5f), Mathf.CeilToInt(FogDataSize.y / 5f), Mathf.CeilToInt(FogDataSize.z / 5f));

        FogMaterial.SetTexture("_FogAcc", LightAccum);
        FogMaterial.SetVector("CameraDir", transform.forward);
        FogMaterial.SetMatrix("_invProj", (cam.projectionMatrix * cam.worldToCameraMatrix).inverse);
    }
    

    void Awake() {
        Extinction = Color.white - Absorbtion;
    }

    private void FixedUpdate() {
        RunShader();
    }
    
    // Postprocess the image
    void OnRenderImage(RenderTexture source, RenderTexture destination) {

        Shader.SetGlobalTexture("_FogAcc", LightAccum);
        Color c = light.intensity * light.color;
        Shader.SetGlobalVector("_LightColor", c);
        FogMaterial.SetTexture("_InjectedShadowMap", directional.shadowMapRenderTexture);
        FogMaterial.SetVector("CameraDir", transform.forward);
        FogMaterial.SetMatrix("_invProj",(cam.projectionMatrix * cam.worldToCameraMatrix).inverse);
        Graphics.Blit(null, FogRender, FogMaterial);
        FogBlit.SetTexture("_FogTex", FogRender);
        FogBlit.SetVector("_UV_PPX", new Vector4(1f / fogTextureSize.x, 1f / fogTextureSize.y, 0, 0));
        Graphics.Blit(source, destination, FogBlit);
    }
}
