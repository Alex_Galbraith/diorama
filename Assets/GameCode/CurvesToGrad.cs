﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
/**
 * Class for converting 4 color channel curves into a pixel gradient strip.
 * Intended for use in artisticly directed fog phase function.
 * Based on: https://github.com/slipster216/CurveTexture/blob/master/CurveTexture.cs
 */

public class CurvesToGrad : ScriptableWizard
{
    [Tooltip("Output path relative to Assets folder.")]
    public string OutputPath = "Gradient.png";

    [Tooltip("Use absolute path?")]
    public bool AbsolutePath = false;
    [Tooltip("Mirror output?")]
    public bool Mirror = false;
    [Tooltip("Red channel curve")]
    public AnimationCurve R = AnimationCurve.Linear(0, 0, 1, 1);
    [Tooltip("Green channel curve")]
    public AnimationCurve G = AnimationCurve.Linear(0, 0, 1, 1);
    [Tooltip("Blue channel curve")]
    public AnimationCurve B = AnimationCurve.Linear(0, 0, 1, 1);
    [Tooltip("Alpha channel curve")]
    public AnimationCurve A = AnimationCurve.Linear(0, 0, 1, 1);
    [Min(1)]
    [Tooltip("Width of the outputted texture. Mirror = true will double this")]
    public int Width = 256;

    [MenuItem("Textures/Gradient From Curves")]
    static void CreateWizard() {
        ScriptableWizard.DisplayWizard<CurvesToGrad>("Curves to Gradient");
    }

    void OnWizardCreate() {
        Texture2D tex = new Texture2D(Mirror?Width*2:Width, 1, TextureFormat.RGBA32, false, true);
        
        Color[] colors = new Color[Mirror ? Width * 2 : Width];
        for (int i = 0; i< Width; i++) {
            float t = i / (float)Width;
            Color c = new Color(R.Evaluate(t), G.Evaluate(t), B.Evaluate(t), A.Evaluate(t));
            colors[i] = c;
        }

        if (Mirror) {
            for (int i = 0; i < Width; i++) {
                float t = i / (float)Width;
                Color c = new Color(R.Evaluate(t), G.Evaluate(t), B.Evaluate(t), A.Evaluate(t));
                colors[Width*2-i-1] = c;
            }
        }

        tex.SetPixels(0, 0, Mirror ? Width * 2 : Width, 1, colors);
        tex.Apply();
        SaveTextureToFile(tex, OutputPath);
        AssetDatabase.Refresh();
        tex = null;
    }
    //From https://answers.unity.com/questions/245600/saving-a-png-image-to-hdd-in-standalone-build.html
    void SaveTextureToFile(Texture2D texture, string fileName) {
        byte[] bytes = texture.EncodeToPNG();
        FileStream file = File.Open(AbsolutePath?"":(Application.dataPath + "/") + fileName, FileMode.Create);
        BinaryWriter binary = new BinaryWriter(file);
        binary.Write(bytes);
        file.Close();
    }
}


#endif