﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(ThreeGrid))]
public class ThreeGridEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        ThreeGrid myScript = (ThreeGrid)target;
        if (GUILayout.Button("Build Object")) {
            myScript.Rebuild();
        }
    }
}
