﻿//1. Transparent
//2. Rim
//3. Intersection Highlight
Shader "Custom/Oily"
{
	Properties
	{
		_MainColor("Main Color", Color) = (1,1,1,1)
		_RimPower("Rim Power", Range(0, 1)) = 1
		_IntersectionPower("Intersect Power", Range(0, 1)) = 0
		_FlowField("Flow", 2D) = "white"
		_NoiseTex("Noise", 2D) = "black"
		_NormTex("Norm", 2D) = "bump"
		_IntersectionBias("Intersection Bias", Float) = 1
	}
		SubShader
	{
		Pass{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }


		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
//#pragma surface surf Standard vertex:vert fullforwardshadows alpha:fade
//#pragma target 3.0

#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"
		
		struct appdata
		{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
		float3 normal : NORMAL;
		};

	

	sampler2D _CameraDepthTexture;
	sampler2D _FlowField;
	sampler2D _NoiseTex, _NormTex;
	fixed4 _MainColor;
	float _RimPower;
	float _IntersectionPower, _IntersectionBias;
	/*
	struct Input {
		//float3 viewDir;
		float4 screenPos;
		float2 uv_MainTex;
		float eyeDepth;
	};*/

	
	struct v2f
	{
		float4 vertex : SV_POSITION;
		float3 worldNormal : TEXCOORD0;
		float3 worldViewDir : TEXCOORD1;
		float4 screenPos : TEXCOORD2;
		float eyeZ : TEXCOORD3;
		float2 uv : TEXCOORD4;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
		o.worldNormal = UnityObjectToWorldDir(v.normal);
		o.worldViewDir = UnityWorldSpaceViewDir(worldPos);
		o.screenPos = ComputeScreenPos(o.vertex);
		COMPUTE_EYEDEPTH(o.eyeZ);
		o.uv = v.uv;
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		float2 flow = float2(-1,1);
		float wrappedTime = frac(_Time * 1);
		float noise = tex2D(_NoiseTex, i.uv + flow * wrappedTime);
		float3 addNorm = tex2D(_NormTex, i.uv + flow * noise);
		float3 worldNormal = normalize(i.worldNormal + addNorm);
		float3 worldViewDir = normalize(i.worldViewDir);
		
		
		float rim = 1 - saturate(dot(worldNormal, worldViewDir)) * _RimPower;
		//half3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
		half3 worldRefl = reflect(-worldViewDir, i.worldNormal);

		// same as in previous shader
		half4 skyData = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldRefl);
		half3 skyColor = DecodeHDR(skyData, unity_SpecCube0_HDR);

		float screenZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)));
		float intersect = (_IntersectionBias - (screenZ - i.eyeZ)) * _IntersectionPower;
		float v = max(rim, intersect);
		float4 c = float4(skyColor,0) + _MainColor * v;
		c = clamp(c, 0, 1);
		return c;
	}
	/*
	void vert(inout appdata_full v, out Input o)
	{
		UNITY_INITIALIZE_OUTPUT(Input, o);
		COMPUTE_EYEDEPTH(o.eyeDepth);
	}

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		//fixed3 flow = fixed3(-1,0,1);//tex2D(_FlowField, IN.uv1);
		float3 f = tex2D(_MainTex, IN.uv_MainTex);

		//float3 worldViewDir = normalize(IN.viewDir);
		//float rim = 0;// -saturate(worldViewDir.y) * _RimPower;



		float rawZ = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(IN.screenPos));
		float sceneZ = LinearEyeDepth(rawZ);
		float objZ = IN.eyeDepth;
		float intersect = (_IntersectionBias1 - (sceneZ - objZ));// *_IntersectionPower;
		float v = intersect;// max(rim, intersect);

		

		o.Albedo = fixed3(1, 0, 0) * (f+v);//_MainColor * (v + _RimPower);
		// Metallic and smoothness come from slider variables
		//o.Metallic = _Metallic;
		//o.Smoothness = _Glossiness;
		//o.Alpha = (v + _RimPower);
	}
	*/
		
		ENDCG
	}
	}
	
}