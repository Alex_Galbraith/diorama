﻿
Shader "Unlit/VolumeSecond"
{
	Properties
	{


		_Center("Position", Vector) = (0,0,0,0)
		//_Noise("Noise", 2D) = "white"{}
		_AmbientColor("Ambient", Color) = (0,0,0,0)
		_FogAcc("FogAcc", 3D) = "transparent"{}
		_Dimensions("Dimensions", Vector) = (25,50,25)
		_BlueNoise("BlueNoise", 2D) = "black"{}
		_Offset("Offset", Float) = 0

	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Cull Off ZWrite Off ZTest Always
		Blend One OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#define SHADOWS_NATIVE
			#pragma multi_compile_fwdbase
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Noise.cginc"
			//#pragma multi_compile _ SHADOWS_NATIVE

			UNITY_DECLARE_SHADOWMAP(_ShadowMapTexture);
			float3 _FogPos;
			fixed4 _Noise_ST, _AmbientColor;
			float3 _Dimensions;
			float3 CameraDir;
			float3 _Pos = float3(0,0,0);

			float4x4 _invProj = 0;


			sampler2D _CameraDepthTexture, _MainTex;
			sampler3D _Fog, _FogAcc;

			struct box{
				float3 min, max;
			};
			struct ray{
				float3 x0;
				float3 n;
			};

			float2 intersection(box b, ray r) {
				float tx1 = (b.min.x - r.x0.x)/r.n.x;
				float tx2 = (b.max.x - r.x0.x)/r.n.x;
				
				float tmin = min(tx1, tx2);
				float tmax = max(tx1, tx2);
 
				float ty1 = (b.min.y - r.x0.y)/r.n.y;
				float ty2 = (b.max.y - r.x0.y)/r.n.y;
 
				tmin = max(tmin, min(ty1, ty2));
				tmax = min(tmax, max(ty1, ty2));

				float tz1 = (b.min.z - r.x0.z)/r.n.z;
				float tz2 = (b.max.z - r.x0.z)/r.n.z;
 
				tmin = max(tmin, min(tz1, tz2));
				tmax = min(tmax, max(tz1, tz2));
 
				return float2(tmin, tmax);
			}

			float2 intersectionfast(box b, ray r) {
				float tx1 = (b.min.x - r.x0.x)*r.n.x;
				float tx2 = (b.max.x - r.x0.x)*r.n.x;
				
				float tmin = min(tx1, tx2);
				float tmax = max(tx1, tx2);
 
				float ty1 = (b.min.y - r.x0.y)*r.n.y;
				float ty2 = (b.max.y - r.x0.y)*r.n.y;
 
				tmin = max(tmin, min(ty1, ty2));
				tmax = min(tmax, max(ty1, ty2));

				float tz1 = (b.min.z - r.x0.z)*r.n.z;
				float tz2 = (b.max.z - r.x0.z)*r.n.z;
 
				tmin = max(tmin, min(tz1, tz2));
				tmax = min(tmax, max(tz1, tz2));
 
				return float2(tmin, tmax);
			}

			bool inBox(box b, float3 p) {
				float tx1 = (b.min.x - p.x);
				float tx2 = (b.max.x - p.x);
				
				float tmin = min(tx1, tx2);
				float tmax = max(tx1, tx2);
 
				float ty1 = (b.min.y - p.y);
				float ty2 = (b.max.y - p.y);
 
				tmin = max(tmin, min(ty1, ty2));
				tmax = min(tmax, max(ty1, ty2));

				float tz1 = (b.min.z - p.z);
				float tz2 = (b.max.z - p.z);
 
				tmin = max(tmin, min(tz1, tz2));
				tmax = min(tmax, max(tz1, tz2));
 
				return (tmin<=0 && tmax>=0);
			}

			float nrand(float2 uv)
			{
				return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453);
			}

			#define STEPS 50
			#define MIN_DISTANCE 1

			#define FIX_FOG_BOUNDARIES 1


			struct ShadowProxy {
				unityShadowCoord4 _ShadowCoord : TEXCOORD0;
			};
			


			fixed4 raymarch(float3 position, float3 direction, float maxDepth, float2 uv, box b)
			{

				float dist = 0;
				float4 accum = 0;

				bool been_in = 0;

				float3 add = 0;
				float adda = 0;
				int mult = 1;

				ray r;
				//Inverse ray for intersectionfast
				r.n = float3(1. / direction.x, 1. / direction.y, 1. / direction.z);
				ShadowProxy sp;
				for (int i = 0; i < STEPS; i++)
				{


					float3 upos = float3(position + _FogPos);
					sp._ShadowCoord = mul(unity_WorldToShadow[0], float4(upos, 1));
					r.x0 = position;
					//Bounds check our volume
					float2 inter = intersectionfast(b, r);
					if ((inter.x <= 0 && inter.y >= 0)) {
						//Flag that the raycast has entered the volume at some point
						been_in = 1;
						//Grab our fog color and density using tex3Dlod to avoid gradient function calls
						float4 acc = tex3Dlod(_FogAcc, float4(upos.x / _Dimensions.x, upos.y / _Dimensions.y, upos.z / _Dimensions.z, 100));

						float4 coord = sp._ShadowCoord;
						fixed shadow = UNITY_SAMPLE_SHADOW(_ShadowMapTexture, coord);
						shadow = _LightShadowData.r + shadow * (1 - _LightShadowData.r);

						acc *= shadow * mult;
						//Calculate how much we should be adding base on distance, density and ambient light
						add = (acc.rgb * MIN_DISTANCE + _AmbientColor.rgb * MIN_DISTANCE* acc.a * _AmbientColor.a)*(1 - accum.a);
						accum.rgb += add;
						adda = acc.a*MIN_DISTANCE*(1 - accum.a);
						accum.a += adda;

						/*
						//Our fog is fuly opaque so no more light can be getting through thus we can stop.
						if (accum.a >= 1 ) {
						#if FIX_FOG_BOUNDARIES
						float remain = (accum.a - 1)/(adda);
						accum.a = 1;
						accum.rgb -= remain * add;
						#endif
						mult = 0;
						break;
						}*/

						//We have hit something opaque or maxed out our distance.
						if (dist>maxDepth) {
							float remain = (dist - maxDepth);
#if FIX_FOG_BOUNDARIES
							accum.rgb -= remain * add / MIN_DISTANCE;
							accum.a -= adda * remain / MIN_DISTANCE;
#endif
							mult = 0;
							break;
						}

						position += MIN_DISTANCE * direction;
						dist += MIN_DISTANCE;

					}
					else {
						//We arent in our fog volume now, but we were before. Thus we have gone out the other side and can stop.
						if (been_in) {
#if FIX_FOG_BOUNDARIES
							accum.rgb += inter.y * add / MIN_DISTANCE;
							accum.a += adda * inter.y / MIN_DISTANCE;
#endif
							mult = 0;
							break;
						}
						//march
						position += MIN_DISTANCE * direction;
						dist += MIN_DISTANCE;
					}
				}
				return saturate(accum);
			}


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float4 pos : SV_POSITION;
				float4 screenPos: TEXCOORD2;
				float eyeZ : TEXCOORD3;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.wpos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.screenPos = ComputeScreenPos(o.pos);
				COMPUTE_EYEDEPTH(o.eyeZ);
				return o;
			}
			

			fixed4 frag(v2f i) : SV_Target
			{
				float3 worldPosition = i.wpos;
				float3 viewDirection = normalize(i.wpos - _WorldSpaceCameraPos);
				

				box b;
				b.max = (_Pos + _Dimensions);
				b.min = (_Pos);

				ray r;
				r.x0 = worldPosition;
				r.n = viewDirection;

				float2 res = intersection(b,r);
				if (res.y < 0 || res.x > res.y)
					return 0;

				float jitter = (nrand(half2(_Time.z*i.uv.x, _Time.z*i.uv.y)))*MIN_DISTANCE;
				float screenZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)));

				fixed4 d = (raymarch(worldPosition + (res.x - jitter)*viewDirection,
					viewDirection,
					screenZ - i.eyeZ - res.x + jitter,  //max depth raycasting should travel
					i.uv, b));

				d = clamp(d, 0, 1);
				return d;
			}


			



			ENDCG
		}
	}
}
