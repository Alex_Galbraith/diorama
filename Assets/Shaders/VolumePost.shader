﻿
Shader "Fog/VolumePost"
{
	Properties
	{

		_MainTex ("Screen Texture", 2D) = "white" {}
		_AmbientColor("Ambient", Color) = (0,0,0,0)
		//_FogAcc("FogAcc", 3D) = "transparent"{}
		_Dimensions("Dimensions", Vector) = (25,50,25)
		_FogPos("Fog Position", Vector) = (0,0,0,0)
		_Extinction("Extinction coefficient", Float) = 1
		_Reflectance("Reflectance coefficient", Float) = 0.5

	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Cull Off ZWrite Off ZTest Off

		LOD 100
		Blend One Zero

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#define SHADOWS_NATIVE
			#pragma multi_compile_lightpass
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Noise.cginc"
			//#pragma multi_compile _ SHADOWS_NATIVE
			
			UNITY_DECLARE_SHADOWMAP(_ShadowMapTexture);
			float3 _FogPos;
			fixed4 _Noise_ST, _AmbientColor;
			float3 _Dimensions;
			float3 CameraDir;
			float3 _Pos = float3(0,0,0);
			fixed _Extinction, _Reflectance;

			float4 _LightColor;

			float4x4 _invProj = 0;
			
			
			sampler2D _CameraDepthTexture, _MainTex;
			sampler2D _InjectedShadowMap;
			sampler3D _Fog, _FogAcc;

			struct box{
				float3 min, max;
			};
			struct ray{
				float3 x0;
				float3 n;
			};
			//ray aabb intersection via clipping planes method
			float2 intersection(box b, ray r) {
				float tx1 = (b.min.x - r.x0.x)/r.n.x;
				float tx2 = (b.max.x - r.x0.x)/r.n.x;
				
				float tmin = min(tx1, tx2);
				float tmax = max(tx1, tx2);
 
				float ty1 = (b.min.y - r.x0.y)/r.n.y;
				float ty2 = (b.max.y - r.x0.y)/r.n.y;
 
				tmin = max(tmin, min(ty1, ty2));
				tmax = min(tmax, max(ty1, ty2));

				float tz1 = (b.min.z - r.x0.z)/r.n.z;
				float tz2 = (b.max.z - r.x0.z)/r.n.z;
 
				tmin = max(tmin, min(tz1, tz2));
				tmax = min(tmax, max(tz1, tz2));
 
				return float2(tmin, tmax);
			}
			//Pre inversed ray aabb intersection, slightly
			//faster because no divisions
			float2 intersectionfast(box b, ray r) {
				float tx1 = (b.min.x - r.x0.x)*r.n.x;
				float tx2 = (b.max.x - r.x0.x)*r.n.x;
				
				float tmin = min(tx1, tx2);
				float tmax = max(tx1, tx2);
 
				float ty1 = (b.min.y - r.x0.y)*r.n.y;
				float ty2 = (b.max.y - r.x0.y)*r.n.y;
 
				tmin = max(tmin, min(ty1, ty2));
				tmax = min(tmax, max(ty1, ty2));

				float tz1 = (b.min.z - r.x0.z)*r.n.z;
				float tz2 = (b.max.z - r.x0.z)*r.n.z;
 
				tmin = max(tmin, min(tz1, tz2));
				tmax = min(tmax, max(tz1, tz2));
 
				return float2(tmin, tmax);
			}
			//Unused point in box check
			bool inBox(box b, float3 p) {
				float tx1 = (b.min.x - p.x);
				float tx2 = (b.max.x - p.x);
				
				float tmin = min(tx1, tx2);
				float tmax = max(tx1, tx2);
 
				float ty1 = (b.min.y - p.y);
				float ty2 = (b.max.y - p.y);
 
				tmin = max(tmin, min(ty1, ty2));
				tmax = min(tmax, max(ty1, ty2));

				float tz1 = (b.min.z - p.z);
				float tz2 = (b.max.z - p.z);
 
				tmin = max(tmin, min(tz1, tz2));
				tmax = min(tmax, max(tz1, tz2));
 
				return (tmin<=0 && tmax>=0);
			}
			//From: https://gist.github.com/keijiro/ee7bc388272548396870
			//Random num gen for noise
			float nrand(float2 uv)
			{
				return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453);
			}

			#define STEPS 50
			#define MIN_DISTANCE 1

			#define VOLUME_REC_SHADOWS 1

			struct ShadowProxy {
				unityShadowCoord4 _ShadowCoord : TEXCOORD0;
			};


			fixed4 raymarch(float3 position, float3 direction, float maxDepth, float2 uv, box b)
			{
				
				float dist = 0;
				float4 accum = float4(0,0,0,1);

				float3 add=0;
				float adda=0;

				for (int i = 0; i < STEPS; i++)
				{
					//Calculate march distance
					float D = max(0, min(maxDepth - dist - MIN_DISTANCE, MIN_DISTANCE));
					if (D == 0)
						return saturate(accum);

					float3 upos = float3(position+_FogPos);
					
					//Grab our fog color and density using tex3Dlod to avoid gradient function calls
					float4 acc = tex3Dlod(_FogAcc, float4(upos.x / _Dimensions.x, upos.y / _Dimensions.y, upos.z / _Dimensions.z, 100)) * _LightColor;
					acc.rgb *= acc.a * _Reflectance;
					
					//Shadow receiving
					#if VOLUME_REC_SHADOWS
					float4 coord = mul(unity_WorldToShadow[0], float4(upos, 1));
					fixed shadow = tex2Dlod(_InjectedShadowMap, float4(coord.xy, 100, 100)).r;
					shadow = step(shadow, coord.z);
					acc.rgb *= shadow;
					#endif

					//Calculate how much we should be adding base on distance, density and ambient light
					add = (acc.rgb * D + _AmbientColor.rgb * D* acc.a * _AmbientColor.a * _Reflectance)*(accum.a);
					accum.rgb += add;
					float newAccA = accum.a * exp(-acc.a*D*_Extinction);
					accum.a = newAccA;

					//march
					position += D * direction;
					dist += D;
				}
				return saturate(accum);
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float4 pos : SV_POSITION;
				float4 screenPos: TEXCOORD2;
				float eyeZ : TEXCOORD3;
			};

			v2f vert (appdata v)
			{
				v2f o;
				
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				v.vertex.x = (1 - v.vertex.x)*2 - 1;
				v.vertex.y = (1 - v.vertex.y)*2 - 1;
				float near = _ProjectionParams.y;


				float4 vpos = float4(v.vertex.x, v.vertex.y, 1, 1);

				// Perspective: view space vertex position of the near plane
				float3 rayPers = mul(unity_CameraInvProjection, vpos * near).xyz;
				rayPers = mul(unity_CameraToWorld, float4(rayPers,1));
				o.wpos = rayPers;


				o.screenPos = ComputeScreenPos(o.pos);
				COMPUTE_EYEDEPTH(o.eyeZ);
				return o;
			}
			

			fixed4 frag(v2f i) : SV_Target
			{
				
				float3 worldPosition = i.wpos.xyz;
				float3 viewDirection = -normalize(worldPosition - _WorldSpaceCameraPos.xyz);
				box b;
				b.max = (_Pos + _Dimensions);
				b.min = (_Pos);

				ray r;
				r.x0 = worldPosition;
				r.n = viewDirection;

				//Check if we are actualy intersecting our volume
				float2 res = intersection(b,r);
				if (res.y < 0 || res.x > res.y)
					return float4(0,0,0,1);

				//jitter samples
				float screenZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)));
				
				float3 camera_forward = mul(unity_CameraToWorld, float4(0,0,1,0)).xyz;

				//calculate cos theta, the cosine of the angle between the forward view direction, and the fragment
				half theta = dot(viewDirection.xyz, camera_forward);
				//calculate the distance from the camera to the fragment using trig
				screenZ = screenZ/abs(theta);

				float jitter = (nrand(half2(_Time.z*i.uv.x,_Time.z*i.uv.y)))*MIN_DISTANCE;
				float maxZ = min(screenZ - i.eyeZ - max(res.x, 0) + jitter, res.y - max(res.x, 0) + jitter);
				//Raymarch. 
				//res.x provides the distance till collision with the fog volume
				//screenZ contains the depth of this fragment which we use to stop raycasting once an opaque surface is hit
				fixed4 d = (raymarch(worldPosition + (max(res.x,0)-jitter)*viewDirection,
					viewDirection, 
					maxZ,  //max depth raycasting should travel
					i.uv, b));
			
				d = clamp(d, 0, 1);
				return d;

			}
			
			ENDCG
		}
	}
}
