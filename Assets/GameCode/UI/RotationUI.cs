﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace UI
{
    public class RotationUI : MonoBehaviour
    {
        [System.Serializable]
        public class RotEvent : UnityEvent<Vector3> {}
        public Slider X, Y, Z;
        public RotEvent OnChange;
        private bool ignoreX, ignoreY, ignoreZ;
        // Start is called before the first frame update
        void Start()
        {
            X.onValueChanged.AddListener(XSet);
            X.onValueChanged.AddListener(XSet);
            X.onValueChanged.AddListener(XSet);
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void SetRotationVisual(Vector3 e){
            ignoreX = ignoreY = ignoreZ = true;
            X.value = e.x;
            Y.value = e.y;
            Z.value = e.z;
            Debug.Log("Set");
        }

        public void XSet(float x){
            if (ignoreX){
                ignoreX = false;
                return;
            }
            OnChange.Invoke(new Vector3(X.value,Y.value,Z.value));
        }

        public void YSet(float y){
            if (ignoreY){
                ignoreY = false;
                return;
            }
            OnChange.Invoke(new Vector3(X.value,Y.value,Z.value));
        }

        public void ZSet(float z){
            if (ignoreZ){
                ignoreZ = false;
                return;
            }
            OnChange.Invoke(new Vector3(X.value,Y.value,Z.value));
        }
    }
}

