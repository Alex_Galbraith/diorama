﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRoamCamera : MonoBehaviour
{
    public float speed = 1;
    public float crollSpeed = 1;
    public float sensitivity = 1;
    public bool invert = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float forward = Input.GetAxis("Vertical");
        float right = Input.GetAxis("Horizontal");
        float up = Input.GetAxis("Jump");

        bool middleClicked = Input.GetButton("Fire3");
        bool rightClicked = Input.GetButton("Fire2");

        transform.Translate(forward * Vector3.forward * speed);
        transform.Translate(right * Vector3.right * speed);
        transform.Translate(up * Vector3.up * speed);

        if (rightClicked) {
            Quaternion yQuaternion = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * sensitivity * (invert ? -1 : 1), Vector3.left);
            Quaternion xQuaternion = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * sensitivity, Vector3.up);
            transform.localRotation = transform.localRotation * yQuaternion;
            transform.localRotation = xQuaternion * transform.localRotation;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else if (middleClicked){
            transform.Translate(Vector3.up * -Input.GetAxis("Mouse Y") * sensitivity);
            transform.Translate(Vector3.right * -Input.GetAxis("Mouse X") * sensitivity);
            Cursor.lockState = CursorLockMode.Locked;
        }
        else {
            Cursor.lockState = CursorLockMode.None;
        }

        transform.Translate(Input.mouseScrollDelta.y * Vector3.forward);
    }
}
